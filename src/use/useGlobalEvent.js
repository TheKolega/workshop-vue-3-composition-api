import { onMounted, onUnmounted } from "vue"
// import mitt from "mitt"

export function useGlobalEvent(event, callback) {
  // const emmitter = mitt()

  onMounted(() => {
    // emmitter.on(event, callback)
    document.body.addEventListener(event, callback)
  })

  onUnmounted(() => {
    // emmitter.off(event, callback)
    document.body.removeEventListener(event, callback)
  })
}
